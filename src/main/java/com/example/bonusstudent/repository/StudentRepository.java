package com.example.bonusstudent.repository;

import com.example.bonusstudent.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student,Integer> {
}
