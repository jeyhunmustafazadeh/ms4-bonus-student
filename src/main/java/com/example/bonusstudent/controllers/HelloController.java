package com.example.bonusstudent.controllers;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("hello")
public class HelloController {

    @GetMapping("/{name}")
    public String helloWorld(@PathVariable String name){
        return "Hello "+ name+" !";
    }

}
