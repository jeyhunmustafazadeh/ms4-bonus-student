package com.example.bonusstudent.controllers;


import com.example.bonusstudent.dto.StudentDto;
import com.example.bonusstudent.models.Student;

import com.example.bonusstudent.services.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.extern.slf4j.XSlf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("students")
public class StudentController {

    private final StudentService studentService;

    @PostMapping
    public ResponseEntity<StudentDto>create(@RequestBody StudentDto studentDto ){
        log.trace("Create student : {}", studentDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(studentService.save(studentDto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<StudentDto>update(@PathVariable int id,@RequestBody StudentDto studentDto){
        log.trace("Uptade student : {}", studentDto);
        return ResponseEntity.ok(studentService.update(id,studentDto));
    }

    @GetMapping
    public ResponseEntity<List<StudentDto>>getAll(){
        log.trace("Get all students . ");
        return ResponseEntity.ok(studentService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<StudentDto>get(@PathVariable int id){
        log.trace("Get student by id : {}", id);
        return ResponseEntity.ok(studentService.findById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void>delete(@PathVariable int id){
        log.trace("Delete student by id : {}", id);
        studentService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
