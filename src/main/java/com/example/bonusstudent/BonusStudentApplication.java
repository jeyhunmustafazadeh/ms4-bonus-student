package com.example.bonusstudent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BonusStudentApplication {

    public static void main(String[] args) {
        SpringApplication.run(BonusStudentApplication.class, args);
    }

}
