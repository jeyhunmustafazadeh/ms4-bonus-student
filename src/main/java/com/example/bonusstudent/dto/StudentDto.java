package com.example.bonusstudent.dto;

import java.time.LocalDate;

public class StudentDto {
    private int id;
    private String name;
    private String institute;
    private LocalDate birthDate;
}
