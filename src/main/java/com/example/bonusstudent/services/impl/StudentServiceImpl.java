package com.example.bonusstudent.services.impl;

import com.example.bonusstudent.dto.StudentDto;
import com.example.bonusstudent.mapper.StudentMapper;
import com.example.bonusstudent.models.Student;
import com.example.bonusstudent.repository.StudentRepository;
import com.example.bonusstudent.services.StudentService;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(rollbackOn = Exception.class)
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    private final StudentMapper studentMapper= Mappers.getMapper(StudentMapper.class);

    @Override
    public StudentDto save(StudentDto studentDto) {
        Student student=studentMapper.toStudent(studentDto);
        StudentDto saved=studentMapper.toStudentDto(studentRepository.save(student));
        return saved;
    }

    @Override
    public StudentDto update(int id, StudentDto studentDto) {
        Student student=studentMapper.toStudent(studentDto);
        student.setId(id);
        return studentMapper.toStudentDto(studentRepository.save(student));
    }

    @Override
    public void delete(int id) {
        Optional<Student>optionalStudent=studentRepository.findById(id);
        if(optionalStudent.isPresent()){
             studentRepository.delete(optionalStudent.get());
        }
        throw new RuntimeException("Student not found !");
    }

    @Override
    public List<StudentDto> findAll() {
        List<Student>students=studentRepository.findAll();
        return studentMapper.toStudentDtos(students);
    }

    @Override
    public StudentDto findById(int id) {
        Optional<Student>optionalStudent=studentRepository.findById(id);
        if(optionalStudent.isPresent()){
            return studentMapper.toStudentDto(optionalStudent.get());
        }
        throw new RuntimeException("Student not found !");
    }
}
