package com.example.bonusstudent.services;


import com.example.bonusstudent.dto.StudentDto;
import java.util.List;

public interface StudentService {
    StudentDto save(StudentDto studentDto);
    StudentDto update(int id,StudentDto studentDto);
    void delete(int id);
    List<StudentDto>findAll();
    StudentDto findById(int id);
}
