package com.example.bonusstudent.mapper;

import com.example.bonusstudent.dto.StudentDto;
import com.example.bonusstudent.models.Student;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface StudentMapper {
    Student toStudent(StudentDto studentDto);
    StudentDto toStudentDto(Student student);
    List<StudentDto> toStudentDtos(List<Student> studentList);
}
